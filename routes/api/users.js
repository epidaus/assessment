const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const key = require('../../config/keys').secret;
const User = require('../../model/User');

//Register
router.post('/register', async (req, res) => {
    let {
        name,
        username,
        email,
        password,
        confirm_password,
        phone,
        dob,
        address,
        status
    } = req.body
    if (password !== confirm_password) {
        return res.status(400).json({
            msg: "Password do not match."
        });
    }

    try {        
        await User.findOne({
            username: username
        }).then(user => {
            if (user) {
                throw 'Username is already taken.'
            }
        })
        await User.findOne({
            email: email
        }).then(user => {
            if (user) {
                throw 'Email is already registered.'
            }
        })
        
    } catch (error) {
        return res.status(400).json({
            msg: error
        }); 
    }
    
    let newUser = new User({
        name,
        username,
        password,
        email,
        phone,
        dob,
        address,
        status
    });
    // Hash the password
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save().then(user => {
                return res.status(201).json({
                    success: true,
                    msg: "Successfully registered."
                });
            });
        });
    });
});


//Login
router.post('/login', (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(user => {
        if (!user) {
            return res.status(404).json({
                msg: "Username not found.",
                success: false
            });
        }
        // compare the password
        bcrypt.compare(req.body.password, user.password).then(isMatch => {
            if (isMatch) {
                // JSON Token for that user
                const payload = {
                    _id: user._id,
                    username: user.username,
                    name: user.name,
                    email: user.email
                }
                jwt.sign(payload, key, {
                    expiresIn: 604800
                }, (err, token) => {
                    res.status(200).json({
                        success: true,
                        token: `Bearer ${token}`,
                        user: user,
                        msg: "You are now logged in."
                    });
                })
            } else {
                return res.status(404).json({
                    msg: "Incorrect password.",
                    success: false
                });
            }
        })
    });
});


//View Profile
router.get('/profile', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    return res.json({
        user: req.user
    });
});

//List all Users
router.get('/userlist', passport.authenticate('jwt', {
    session: false
}), function(req, res) {
    User.find({}, function(err, users) {
      var userMap = {};
  
      users.forEach(function(user) {
        userMap[user._id] = user;
      });
  
      res.send(userMap);  
    });
  });

//Delete
router.delete('/delete/:id', async (req, res) => {
    // console.log(req.params.id)
    try {
        await User.deleteOne({
            _id: req.params.id
        }).then((response) => {
            if(response.deletedCount === 1){
                return res.status(200).json({
                    msg : 'Successfully Deleted!'
                })
            } else {
                return res.status(401).json({
                    msg : 'User not exist!'
                })
            }
        });
        
    } catch (error) {
        return res.status(400).json({
            msg: 'User not exist!'
        }); 
    }
});

//Add Profile Info
router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
  await User.updateOne({ 
      _id: req.params.id 
    }, 
    { 
        ...req.body 
    }, 
    function(err, result) {
    if (err) {
      res.send(err);
    } else {
      res.json(result);
    }
    });
});

module.exports = router;